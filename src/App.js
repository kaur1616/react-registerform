import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Login } from './app/components/Login';
import UserList from './app/components/UserList';
import { Register } from './app/components/Register';
import './css/style.css';


function App() {
  return (
    <Router>
      <div className="App">
        <div className="row row-hg">
          <div className="col-md-6"></div>
          <div className="col-md-6 right-panel">
            <div class="container">
                <Switch>
                  <Route exact path='/' component={Login} />
                  <Route path="/signin" component={Login} />
                  <Route path="/signup" component={Register} />
                  <Route path="/userlist" render={(props) => <UserList {...props}/>} />
                  <Route>Page Not found</Route>
                </Switch>
            </div>
          </div>
           
        </div>
      </div>
    </Router>
  );
}

export default App;