import React from 'react'
import { Link, Redirect } from 'react-router-dom';
import { decrypt } from '../../utilities/crypto';
import { checkEmailExist, validateForm } from '../../utilities/common';

//LOGIN COMPONENT
export class Login extends React.Component {
  constructor() {
    super();

    this.state = {
      fields: {},
      errors: {},
      message: "",
      isLogin: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.LoginForm = this.LoginForm.bind(this);
  }

  //on chnage 
  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value.trim();
    this.setState({
      fields
    })
  }

  // on form submit 
  LoginForm(e) {
    e.preventDefault();
    let data = validateForm(this.state.fields, "login");
    this.setState({
      errors: data.errors
    });
    if (data.formIsValid) {
      let { email, password } = this.state.fields;

      let { user, status } = checkEmailExist(email);
      if (status) {
        let plainText = decrypt(user.password).trim();
        if (plainText == password.trim()) {
          this.setState({
            message: "Login Successfully!",
            isLogin: true
          });
          localStorage.setItem('authorization', '1234657890');
        } else {
          this.setState({
            message: "Username or password doesn't match!",
            isLogin: false
          })
        }
      } else {
        this.setState({
          message: "Username/Password doesn't exist, Please SIGN UP and try again!",
          isLogin: false
        })
      }
    }
  }




  render() {
    const { message, isLogin } = this.state;
    if (isLogin) {
      return <Redirect to={{
        pathname: "/userlist", state: { isLoggedIn: isLogin }
      }} />
    }
    return (
      <div className="row justify-content-center align-items-center">
        <div className="col-md-6">
          <div className="col-md-12">
            <form onSubmit={this.LoginForm}>
              <center>
                <h4>Welcome back!</h4>
                <h5>Please login to your account.</h5><br />
              </center>

              <div className="form-group">
                <input type="text" name="email" value={this.state.fields.email} className="form-control" placeholder="Email address" onChange={this.handleChange} />
              </div> <p style={{ color: "red" }}>{this.state.errors.email}</p><br />

              <div className="form-group">
                <input type="password" name="password" value={this.state.fields.password} className="form-control" placeholder="Password" onChange={this.handleChange} />
              </div><p style={{ color: "red" }}>{this.state.errors.password}</p>

              <p>
                {isLogin
                  ?
                  <p style={{ color: "green" }}> {message}</p>
                  : <p style={{ color: "red" }}> {message} </p>
                }
              </p>

              <div className="row" style={{ "margin-top": "50px" }}>
                <div className="col-md-6">
                  <button type="submit" className="btn btn-success btn-signin" >Login</button>
                </div>
                <div className="col-md-6">
                  <Link to="/signup" className="btn btn-primary btn-signup">Sign up</Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}