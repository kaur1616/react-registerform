import React from 'react'
import { Redirect } from 'react-router-dom';

export class Logout extends React.Component {
    constructor(props) {
        super(props);

    }

    handleLogOut() {
        localStorage.removeItem("authorization");
        return <Redirect to="/sign in" />
    }

    render() {
        return (
            <a href ="/signin" className="btn btn-primary btn-logout" onClick={this.handleLogOut}>Logout</a>
        )
    }
}
