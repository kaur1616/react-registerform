import React from "react";
import { validateForm } from '../../utilities/common';
import { encrypt } from '../../utilities/crypto';
import { setNewUser } from '../../utilities/common';
import { Redirect } from 'react-router-dom';
export class Register extends React.Component {

    constructor() {
        super();


        this.state = {
            formFields: {},
            formErrors: {},
            isSignup: false
        }

        this.handlerSignup = this.handlerSignup.bind(this);
        this.signupOnChange = this.signupOnChange.bind(this);


    }


    //on click of sign up button
    handlerSignup = e => {
        e.preventDefault();
        let data = validateForm(this.state.formFields, "signup");
        this.setState({
            formErrors: data.errors
        });

        if (data.formIsValid) {
            let { email, password, name, mobile } = this.state.formFields;
            let obj = {
                "name": name,
                "email": email,
                "password": encrypt(password),
                "mobile": mobile,
            }
            setNewUser(obj);
            this.setState({
                isSignup: true
            })
        }
        else {
            this.setState({
                isSignup: false
            })
        }
    }


    //sign up On Change
    signupOnChange = e => {
        e.preventDefault();
        let formFields = this.state.formFields;
        formFields[e.target.name] = e.target.value.trim();
        this.setState({
            formFields
        })
    }


    render() {
        const { isSignup } = this.state;
        if (isSignup) {
            return <Redirect to="/signin" />
        }

        return (
            <div className="row justify-content-center align-items-center">
                <div className="col-md-6">
                    <div className="col-md-12">
                        <form onSubmit={this.handlerSignup}>
                            <h4 style={{ "color": "green", "text-align": "center" }}>Create your account!</h4> <br />
                            <div className="form-group">
                                <label>Name</label>
                                <input type="text" name="name" id="name" className="form-control" placeholder="Name" value={this.state.formFields.name} onChange={this.signupOnChange} />
                            </div> <p style={{ color: "red" }}> {this.state.formErrors.name} </p> <br />
                            <div className="form-group">
                                <label>Email address</label>
                                <input type="text" name="email" id="email" className="form-control" placeholder="Enter email" value={this.state.formFields.email} onChange={this.signupOnChange} />
                            </div> <p style={{ color: "red" }}> {this.state.formErrors.email}</p><br />

                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" name="password" id="password" className="form-control" placeholder="Enter password" value={this.state.formFields.password} onChange={this.signupOnChange} />
                            </div> <p style={{ color: "red" }}> {this.state.formErrors.password}</p><br />

                            <div className="form-group">
                                <label>Mobile</label>
                                <input type="number" name="mobile" id="mobile" className="form-control" placeholder="Enter mobile number" value={this.state.formFields.mobile} onChange={this.signupOnChange} />
                            </div> <p style={{ color: "red" }}> {this.state.formErrors.mobile}</p><br />
                            <div className="row">
                                <button type="submit" className="btn btn-primary btn-signup">Sign Up</button>
                            </div>
                            <div className="row">
                                <div class="form-group">
                                    <p class="text-center already-acc">Already have account? <a href="/signin">Sign in</a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}