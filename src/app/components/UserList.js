
import React from 'react'
import { Redirect } from 'react-router';
import { Login } from './../components/Login';
import { Logout } from './Logout';

const UserList = (props) => {
    let urlLocation = props.location && props.location.state;
    let isShowBtn = urlLocation ? urlLocation.isLoggedIn : false;
    const isLogged = localStorage.getItem('authorization');

    if (!isLogged) {
        return (
            <Login to="/login" />
        )
    } else {
        const userList = JSON.parse(localStorage.getItem('userDetail'));
        if (userList && userList.length) {
            return (
                <div className="row justify-content-center align-items-center">
                    <div className="col-md-8">
                        <div className="col-md-12">
                            <div>
                                {isShowBtn ? <Logout /> : <Redirect to="/signin" />}
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">User Name</th>
                                            <th scope="col">Email address</th>
                                            <th scope="col">Mobile number</th>
                                        </tr>
                                    </thead>
                                    <ListDetail details={userList} />
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            )

        } else {
            <h1>No Record Found!</h1>
        }


    }
}

const ListDetail = (props) => {
    return (
        props.details && props.details.map((list, i) => {
            return (
                <tbody>
                    <tr>
                        <th scope="row">{i + 1}</th>
                        <td>{list.name}</td>
                        <td>{list.email}</td>
                        <td>{list.mobile}</td>
                    </tr>
                </tbody>
            )
        })
    )
}

export default UserList;