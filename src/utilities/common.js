//check Email Exist
export const checkEmailExist = (value) => {
    let users = JSON.parse(localStorage.getItem('userDetail'));
    if (users && users.length) {
        let index = users.map(function (e) { return e.email; }).indexOf(value);

        if (index >= 0) {
            return {
                status: true,
                user: users[index]
            };
        } else {
            return {
                status: false
            };
        }

    } else {
        return {
            status: false
        };
    }
}

//set New User
export const setNewUser = (payload) => {
    let existingDetail = JSON.parse(localStorage.getItem('userDetail'));
    if (existingDetail && existingDetail.length) {
        existingDetail.push(payload);
    } else {
        existingDetail = [payload]
    }
    localStorage.setItem('userDetail', JSON.stringify(existingDetail));
}

// validate Form
export const validateForm = (values, type) => {
    let fields = values;
    let { status } = checkEmailExist(fields["email"]);

    let errors = {};
    let formIsValid = true;

    if (!fields["email"]) {
        formIsValid = false;
        errors["email"] = "*Email address is required.";
    }
    else if (status && type == "signup") {
        formIsValid = false;
        errors["email"] = "Email already exists!";
    }

    else if (!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(fields["email"]) && fields["email"]) {
        formIsValid = false;
        errors["email"] = "*Email address is invalid.";
    }


    if (!fields["password"]) {
        formIsValid = false;
        errors["password"] = "*Password is required.";
    }

    //FOR SIGN UP FIELDS
    if (type == "signup") {
        if (fields["password"] && fields["password"].length < 6) {
            formIsValid = false;
            errors.password = 'Password needs to be 6 characters or more ';
        }
        if (!fields["name"]) {
            formIsValid = false;
            errors.name = '*Name is required';
        }

        if (!fields["mobile"]) {
            formIsValid = false;
            errors.mobile = '*Mobile Number is required';
        }
        else if (!/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(fields["mobile"]) && fields["mobile"]) {
            formIsValid = false;
            errors.mobile = 'Please enter 10 digit mobile number ';
        }
    }


    return ({ formIsValid: formIsValid, errors: errors });
}