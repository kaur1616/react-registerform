import CryptoJS from 'crypto-js';

let secretKey = 'secret key 123';

//Encryption
export function encrypt(message) {
    return CryptoJS.AES.encrypt(message, secretKey).toString();
}


//Decryption
export function decrypt(text) {
    let bytes = CryptoJS.AES.decrypt(text, secretKey);
    return bytes.toString(CryptoJS.enc.Utf8);
}